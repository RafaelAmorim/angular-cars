import { Injectable } from '@angular/core';
import { Feathers } from './feathers.service';

/**
 *  Abstraction layer for data management.
 */
@Injectable()
export class DataService {
  constructor(private feathers: Feathers) {
  }

  things$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('things'))
      .watch()
      .find();
  }

  addThing(desc: string) {
    if (desc === '') {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('things')
      .create({
        desc
      });
  }
  
  cars$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('cars'))
      .watch()
      .find();
  }

  addCar(make: string, model: string, year: number, milleage: number) {
    if (make === '' || model === '' || year === null || milleage === null) {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('cars')
      .create({
        make,
        model,
        year,
        milleage
      });
  }
  
  deleteCar(carId){
    this.feathers
      .service('cars')
      .remove(carId)
  }
}


