import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { Paginated } from '@feathersjs/feathers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CarsComponent implements OnInit {
  cars$: Observable<any[]>;
  carForm;
  
  constructor(
    private data: DataService,
    private formBuilder: FormBuilder
  ) { 
    // get things from data service
    this.cars$ = data.cars$().pipe(
        // our data is paginated, so map to .data
        map((t: Paginated<any>) => t.data)
      );
  }
  
  onSubmit(carData) {
    this.data.addCar( carData.make, carData.model, carData.year, carData.milleage );
  }
  
  onDelete(carData) {
    this.data.deleteCar(carData.id);
  }

  ngOnInit(): void {
    this.carForm = this.formBuilder.group({
      make: ['', [Validators.required] ],
      model: ['', [Validators.required] ],
      year: ['', [Validators.required] ],
      milleage: ['', [Validators.required] ],
    });
  }

}
