import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThingsComponent }      from './things/things.component';
import { CarsComponent }      from './cars/cars.component';

const routes: Routes = [
  { path: 'things', component: ThingsComponent },
  { path: 'cars', component: CarsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
